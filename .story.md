---
focus: src/Sudoku.js
---

### Finish

During this story, you reviewed how to deal with return codes and how to fix CQS violation for different cases. Also,
you saw how very helpful comments became unnecessary once the code was clean. Lastly, you observed that following the DRY
principle makes the code more readable.

<quiz>
  <question>How do you solve a CQS violation correctly?</question>
  <answer>Use exceptions instead of return something</answer>
  <answer>Return null</answer>
  <answer correct>There no single answer, it always depends</answer>
</quiz>
